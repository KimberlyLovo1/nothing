/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DaoImplement;

import Dao.SucursalInterface;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import pojo.Municipio;
import pojo.Sucursal;


/**
 *
 * @author TSIS-11
 */
public class SucursalImplement implements SucursalInterface<Sucursal>{

    final int TAMAÑO=514; //SIZE
    File archivo; // file
    String urlArchivo; // path
    RandomAccessFile flujo; // raf

    public SucursalImplement() {
        urlArchivo="Sucursales.dat";
        archivo= new File(urlArchivo);     
    }
    
    public void AbrirFlujo()throws IOException{
        if(!archivo.exists()){
            archivo.createNewFile();
            flujo = new RandomAccessFile(archivo,"rw");
            flujo.seek(0);
            flujo.writeInt(0); // n
            flujo.writeInt(0); // k
        }else{
           flujo=new RandomAccessFile(archivo,"rw");
         }
    }
    
    public void cerrarFlujo() throws IOException{
        if(flujo !=null){
            flujo.close();
        }
    }
    
    public Sucursal MostrarSucursalxID (int id) throws IOException{
        Sucursal s = null;
        
        AbrirFlujo();
        
        int n = flujo.readInt();
        int k = flujo.readInt();
        
        for (int i = 0; i < n; i++){
            long pos = 8 + i * TAMAÑO;
            flujo.seek(pos);
            int idSucursal = flujo.readInt();
            if (id == idSucursal){
                String nombreSucursal = flujo.readUTF();
                int idMunicipio = flujo.readInt();
                String nomMunicipio = flujo.readUTF();
                String nomDepartamento = flujo.readUTF();
                String direccion = flujo.readUTF();
                String telefono = flujo.readUTF();
                String correo = flujo.readUTF();
                int activo = flujo.readInt();
                String fechaCreacion = flujo.readUTF();
                String fechaModificacion = flujo.readUTF();
                Municipio m = new Municipio(idMunicipio, nomMunicipio, nomDepartamento);
                s = new Sucursal(idSucursal, nombreSucursal, m, direccion, telefono, correo, activo, fechaCreacion, fechaModificacion);
                break; 
            }
        }
        cerrarFlujo();
        return s;
    }
    
    
    @Override
    public void Guardar(Sucursal s) {
        try {
            AbrirFlujo();
            flujo.seek(0);
            int n = flujo.readInt(); 
            int k = flujo.readInt();
            
            long pos= 8+(n* TAMAÑO);
            flujo.seek(pos);
            
            flujo.writeInt(++k);//reemplaza el id de la sucursal
            flujo.writeUTF(arreglarCadena(s.getNombreSucursal(), 30));
            flujo.writeInt(k);
            flujo.writeUTF(arreglarCadena(s.getM().getNombreMunicipio(), 20));
            flujo.writeUTF(arreglarCadena(s.getM().getNombreDepartamento(), 40));
            flujo.writeUTF(arreglarCadena(s.getDireccion(), 75));
            flujo.writeUTF(arreglarCadena(s.getTelefono(), 9));
            flujo.writeUTF(arreglarCadena(s.getCorreo(), 25));
            flujo.writeInt(s.getActivo()); // Al registrarse la sucursal se pone como activa autom.
            Date d = new Date();
            flujo.writeUTF(arreglarCadena(d.toLocaleString(), 20));
            flujo.writeUTF(arreglarCadena(d.toLocaleString(), 20));
            
            flujo.seek(0);
            flujo.writeInt(++n);
            flujo.writeInt(k);
            
            cerrarFlujo();
        } catch (IOException ex) {
            Logger.getLogger(SucursalImplement.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }

    @Override
    public void Modificar(Sucursal s) {
        try {
            AbrirFlujo();
            flujo.seek(0);
            int n = flujo.readInt();
            int k = flujo.readInt();
            
            for(int i = 0; i< n; i++){
                long pos = 8 + i * TAMAÑO;
                int id = flujo.readInt();
                if(s.getIdSucursal() == id){
                    flujo.writeUTF(arreglarCadena(s.getNombreSucursal(), 30));
                    flujo.writeInt(s.getM().getId());
                    flujo.writeUTF(arreglarCadena(s.getM().getNombreMunicipio(), 20));
                    flujo.writeUTF(arreglarCadena(s.getM().getNombreDepartamento(), 40));
                    flujo.writeUTF(arreglarCadena(s.getDireccion(), 75));
                    flujo.writeUTF(arreglarCadena(s.getTelefono(), 9));
                    flujo.writeUTF(arreglarCadena(s.getCorreo(), 25));
                    flujo.writeInt(s.getActivo()); // Al registrarse la sucursal se pone como activa autom.
                    flujo.writeUTF(arreglarCadena(s.getFechaCreacion(), 20));
                    flujo.writeUTF(arreglarCadena(s.getFechaModificacion(), 20));
                }
            }
        } catch (IOException ex) {
            Logger.getLogger(SucursalImplement.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }

    @Override
    public boolean Eliminar(Sucursal t) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Sucursal> mostrarTodos(){
        List<Sucursal> sucursales = new ArrayList<>();
        try{
            AbrirFlujo();
            flujo.seek(0);
            int n = flujo.readInt();
            int k = flujo.readInt();
            
            for (int i = 0; i< n; i++){
                long pos = 8+ i * TAMAÑO;
                flujo.seek(pos);
                int idSucursal = flujo.readInt();
                String nombreSucursal = flujo.readUTF();
                int idMunicipio = flujo.readInt();
                String nomMunicipio = flujo.readUTF();
                String nomDepartamento = flujo.readUTF();
                String direccion = flujo.readUTF();
                String telefono = flujo.readUTF();
                String correo = flujo.readUTF();
                int activo = flujo.readInt();
                String fechaCreacion = flujo.readUTF();
                String fechaModificacion = flujo.readUTF();
                Municipio m = new Municipio(idMunicipio, nomMunicipio, nomDepartamento);
                Sucursal s = new Sucursal(idSucursal, nombreSucursal, m, direccion, telefono, correo, activo, fechaCreacion, fechaModificacion);
                sucursales.add(s);
            }
            cerrarFlujo();
        }catch (IOException ex) {
            Logger.getLogger(SucursalImplement.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return sucursales;
    }
    
    public String arreglarCadena(String texto, int capacidad){
        StringBuilder sb = null;
        if(texto == null){
            sb = new StringBuilder(capacidad);
        }else{
            sb = new StringBuilder(texto);
            sb.setLength(capacidad);
        }
        return sb.toString();
    }  
}
