/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pojo;

/**
 *
 * @author TSIS-11
 */
public class Sucursal {
    int idSucursal;//4 bytes
    String nombreSucursal;//30 caracteres*2+3UTF=63 bytes
    Municipio m;// 130 bytes
    String direccion;//75 caracteres*2+3 UTF = 153 bytes
    String telefono;//9*2+3=21 bytes
    String correo;// 25*2+3=53
    int activo;//0=inactivo; 1=activo//4 bytes
    String fechaCreacion;// 20*2+3=43 bytes
    String fechaModificacion;//20*2+3=43 bytes
    /*
        PESO TOTAL DE UNA SUCURSAL = 514 BYTES
    */

    public Sucursal() {
    }

    public Sucursal(int idSucursal, String nombreSucursal, Municipio m, String direccion, String telefono, String correo, int activo, String fechaCreacion, String fechaModificacion) {
        this.idSucursal = idSucursal;
        this.nombreSucursal = nombreSucursal;
        this.m = m;
        this.direccion = direccion;
        this.telefono = telefono;
        this.correo = correo;
        this.activo = activo;
        this.fechaCreacion = fechaCreacion;
        this.fechaModificacion = fechaModificacion;
    }

    public int getIdSucursal() {
        return idSucursal;
    }

    public void setIdSucursal(int idSucursal) {
        this.idSucursal = idSucursal;
    }

    public String getNombreSucursal() {
        return nombreSucursal;
    }

    public void setNombreSucursal(String nombreSucursal) {
        this.nombreSucursal = nombreSucursal;
    }

    public Municipio getM() {
        return m;
    }

    public void setM(Municipio m) {
        this.m = m;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public int getActivo() {
        return activo;
    }

    public void setActivo(int activo) {
        this.activo = activo;
    }

    public String getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(String fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getFechaModificacion() {
        return fechaModificacion;
    }

    public void setFechaModificacion(String fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }

    @Override
    public String toString() {
        
        return "Sucursal: " + "\nidSucursal = " + idSucursal + "\n nombreSucursal = " + nombreSucursal + "\n Municipio =" + m.getNombreMunicipio() + "\n direccion = " + direccion + "\n telefono = " + telefono + "\n correo = " + correo + "\n activo = " + activo + "\n fechaCreacion = " + fechaCreacion + "\n fechaModificacion = " + fechaModificacion + '}';
    }
    
   
    
    
}
