
import DaoImplement.SucursalImplement;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;
import pojo.Municipio;
import pojo.Sucursal;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author DOCENTE
 */
public class RegistroSucursales {

    /**
     * @param args the command line arguments
     */
    static Scanner sc = new Scanner(System.in);
    static SucursalImplement sucImp = new SucursalImplement();

    public static void main(String[] args) throws IOException {
        // TODO code application logic here
        boolean seguir = true;

        while (seguir) {
            List<Sucursal> sucursales = new ArrayList<>(); //Para guardar regitros

            System.out.println("BIENVENIDO AL SISTEMA DE SUCURSALES GALLO MÁS GALLO");
            int opcMenu = 0;
            do {
                System.out.println("1. Registrar una nueva sucursal");
                System.out.println("2. Modificar información de una sucursal.");
                System.out.println("3. Eliminar una sucursal");
                System.out.println("4. Mostrar sucursal por id");
                System.out.println("5. Mostrar todas las sucursales");
                opcMenu = sc.nextInt();
            } while (opcMenu < 1 || opcMenu > 5);

            switch (opcMenu) {
                case 1: {
                    int opcSeguirGuardando = 1;
                    while (opcSeguirGuardando == 1) {
                        System.out.print("Nombre Sucursal: ");
                        sc.nextLine();
                        String nomSucursal = sc.nextLine();
                        System.out.print("Nombre Municipio:");
                        String nomMunicipio = sc.nextLine();
                        System.out.print("Nombre Departamento:");
                        String nomDepartamento = sc.nextLine();
                        System.out.print("Dirección:");
                        String direccion = sc.nextLine();
                        System.out.print("Teléfono:");
                        String telefono = sc.nextLine();
                        System.out.print("Correo:");
                        String correo = sc.nextLine();
                        Municipio m = new Municipio(0, nomMunicipio, nomDepartamento);
                        Sucursal s = new Sucursal(0, nomSucursal, m, direccion, telefono, correo, 1, "", "");
                        sucursales.add(s);
                        do {
                            System.out.println("¿Desea registrar otra sucursal? 1. SI 0. NO");
                            opcSeguirGuardando = sc.nextInt();
                        } while (opcSeguirGuardando < 0 || opcSeguirGuardando > 1);
                        
                    }
                    
                    for(Sucursal s : sucursales){
                        sucImp.Guardar(s);
                    }
                    
                    break;
                }

                case 2: {
                    int id = 0;
                    do{ //BUSCAMOS LA SUCURSAL POR ID
                    System.out.println("Didite el id de la sucursal a modificar");
                    id = sc.nextInt();
                    
                    }while(id < 1);
                    
                    Sucursal s = sucImp.MostrarSucursalxID(id);
                    if (s != null){
                        System.out.println("A continuacion se modifica los datos de la sucursal: " + s.getNombreSucursal());
                        System.out.println("Si no desea modificar ciertos atributos escriba ¨-¨");
                        System.out.println("Nombre sucursal: ");
                        String nombreSucursal = sc.nextLine();
                        nombreSucursal = (!nombreSucursal.equals("-"))? nombreSucursal : s.getNombreSucursal();
                        System.out.println("Nombre de Municipio: ");
                        String nombreMunicipio = sc.nextLine();
                        nombreMunicipio = (!nombreMunicipio.equals("-"))? nombreMunicipio : s.getM().getNombreMunicipio();
                        System.out.println("Nombre de departamento: ");
                        String nombreDepartamento = sc.nextLine();
                        nombreDepartamento = (!nombreDepartamento.equals("-"))? nombreDepartamento : s.getM().getNombreDepartamento();
                        System.out.println("Dirreccion: ");
                        String direccion = sc.nextLine();
                        direccion = (!direccion.equals("-"))? direccion : s.getDireccion();
                        System.out.println("Telefono: ");
                        String telefono = sc.nextLine();
                        telefono = (!telefono.equals("-"))? telefono : s.getTelefono();
                        System.out.println("Correo: ");
                        String correo = sc.nextLine();
                        correo = (!correo.equals("-"))? correo : s.getCorreo();                       
                        int opcActivo = 0;
                        do{
                            System.out.println("Activo: 0. Inactivo, 1. Activo");
                            opcActivo = sc.nextInt();
                        }while( opcActivo < 0 || opcActivo > 1);
                        String fechaCreacion = s.getFechaCreacion();
                        Date d = new Date();
                        String fechaModificacion = d.toLocaleString();
                        Municipio m = new Municipio(s.getM().getId(), nombreMunicipio, nombreDepartamento);
                        s = new Sucursal (id, nombreSucursal, m, direccion, telefono, correo, opcActivo, fechaCreacion, fechaModificacion);
                        
                        
                                
                    }else {
                        System.err.println("La Sucursal no fue encontrada");
                    }
                    break;
                }

                case 3: {

                    break;
                }

                case 4: {
                    int id = 0;
                    do {
                        System.out.println("Diigite el id de la sucursal que desea ver");
                        id = sc.nextInt();    
                    }while(id < 1);
                     Sucursal s = sucImp.MostrarSucursalxID(id);
                     if( s != null){
                         System.out.println(s.toString());
                     }else {
                         System.err.println("La sucursal no fue encontrada");
                     }
                    
                    break;
                }
                
                case 5: {
                    List<Sucursal> sucursalesTemporal = sucImp.mostrarTodos();
                    
                    for(Sucursal s : sucursalesTemporal){
                        System.out.println(s.toString());
                    }
                    break; 
                }
            }

            int opcSeguir = 0;
            do {
                System.out.println("¿Deseas seguir en el sistema? \n 1. SI  0. NO");
                opcSeguir = sc.nextInt();
            } while (opcSeguir < 0 || opcSeguir > 1);

            if (opcSeguir == 0) {
                seguir = false;
            }
        }
    }

}
