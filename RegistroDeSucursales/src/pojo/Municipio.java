/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pojo;

/**
 *
 * @author TSIS-11
 */
public class Municipio {
    int id;//4 bytes
    String nombreMunicipio;//20 caracteres*2 bytes+3 bytes UTF=43
    String nombreDepartamento;//40 caracteres*2 bytes=8 bytes +3 UTF= 83 bytes
    
    /*
        PESO TOTAL MUNICIPIO = 130 BYTES
    */

    public Municipio() {
    }

    public Municipio(int id, String nombreMunicipio, String nombreDepartamento) {
        this.id = id;
        this.nombreMunicipio = nombreMunicipio;
        this.nombreDepartamento = nombreDepartamento;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombreMunicipio() {
        return nombreMunicipio;
    }

    public void setNombreMunicipio(String nombreMunicipio) {
        this.nombreMunicipio = nombreMunicipio;
    }

    public String getNombreDepartamento() {
        return nombreDepartamento;
    }

    public void setNombreDepartamento(String nombreDepartamento) {
        this.nombreDepartamento = nombreDepartamento;
    }
    
    
}
