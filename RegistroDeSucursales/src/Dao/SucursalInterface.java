/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;

import java.util.List;

/**
 *
 * @author TSIS-11
 */
public interface SucursalInterface<T> {
    public void Guardar(T t);
    public void Modificar(T t);
    public boolean Eliminar(T t);
    // para arreglo public T[] mostrarTodos();
    public List<T> mostrarTodos();
    
}
